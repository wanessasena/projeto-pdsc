//Obter todos os usuários

var request = new XMLHttpRequest();
var allUsersURL = "http://localhost:8080/APIGateway/api/all_users";

request.open("GET", allUsersURL);
request.setRequestHeader("Content-Type", "application/json");
request.send(); 

request.onreadystatechange = function(){
    if(this.readyState === 4 && this.status === 200){
        var userList = [];
        userList = JSON.parse(this.responseText);
        createUsersTables(userList)
    };

}

function createUsersTables(userList){
    var divt = document.createElement("div");

    var str = '<div><table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="tabelausuarios">'
    + '<thead><tr><th>Nome</th><th>E-mail</th><th>Data de Nascimento</th><th>QR Code</th><th>Tipo de Cargo</th><th>Departamento</th><th>Administrador</th><th>Ativo</th></tr></thead>';
    
    var str2;

    if(userList.length == 0){
        str2 = 'Nenhum registro para exibir...'
        str = str + str2;
    }else{
    userList.forEach(function (node){
        str2 = '<tr><td>' + node.nome +'</td><td>' + node.email + '</td><td>' + node.data_nascimento + '</td><td>'+ node.qrcode + '</td><td>' 
                    + node.tipo_cargo_id + '</td><td>' + node.departamento_id + '</td><td>' + node.administrador + '</td><td>' + node.ativo + '</td></tr>';
        
        str = str + str2;
    });
    }
    str = str + '<tbody></table></div>';

    divtable.innerHTML = str;

    document.getElementById("divtable").appendChild(divt);

} 