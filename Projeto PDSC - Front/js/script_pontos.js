//Obter todos os registros de ponto 

var request = new XMLHttpRequest();

var allPontosURL = "http://localhost:8080/APIGateway/api/all_pontos/";

request.open("GET", allPontosURL);
request.setRequestHeader("Content-Type", "application/json");
request.send(); 

request.onreadystatechange = function(){
    if(this.readyState === 4 && this.status === 200){
        var pontosList = [];
        pontosList = JSON.parse(this.responseText);
        createRegistrationTables(pontosList)
    };

}

function createRegistrationTables(pontosList){
    var divt = document.createElement("div");

    var str = '<div><table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="tabelausuarios">'
    + '<thead><tr><th>Usuário</th><th>Ordem</th><th>Registrado Em</th><th>Endereço</th><th>Latitude</th><th>Longitude</th></tr></thead>';
    
    var str2;

    if(pontosList.length == 0){
        str2 = 'Nenhum registro para exibir...'
        str = str + str2;
    }else{
    pontosList.forEach(function (node){
        str2 = '<tr><td>' + node.user_id + '</td><td>' + node.order +'</td><td>' + node.created_on + '</td><td>' + node.address + '</td><td>'+ node.latitude + '</td><td>' 
                    + node.longitude + '</td></tr>';
        
        str = str + str2;
    });
    }

    str = str + '<tbody></table></div>';

    divtable.innerHTML = str;

    document.getElementById("divtable").appendChild(divt);

} 