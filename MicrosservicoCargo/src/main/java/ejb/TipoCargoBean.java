package ejb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entidades.TipoCargo;

@Stateless
public class TipoCargoBean {
	@PersistenceContext(unitName = "pu")
	private EntityManager entityManager;
	
	@PostConstruct
    private void initializeBean(){
    }
	
	public List<TipoCargo> getAllTiposCargo() {
		String jpql = ("select t from TipoCargo t");
        Query query = entityManager.createQuery(jpql, TipoCargo.class);
        List<TipoCargo> tiposcargo = query.getResultList();
        if (tiposcargo !=null) {
        	return tiposcargo;
        }
		return null;
	}
	
	public TipoCargo getTipoCargo(int id) {
		TipoCargo tipocargo = entityManager.find(TipoCargo.class, id);
		if (tipocargo != null)
            return tipocargo;
		return null;
	} 
}
