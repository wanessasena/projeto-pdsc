package api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.transaction.Transactional;
import javax.ejb.EJB;
import ejb.TipoCargoBean;
import entidades.TipoCargo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;

@Path("/tiposcargo")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional	
public class TipoCargoService {
	@EJB
    private TipoCargoBean tipoCargoBean;

	@Context
	private HttpServletRequest httpRequest;
	
    @GET
    public Response allTiposCargo() {
    	List<TipoCargo> tiposcargo = tipoCargoBean.getAllTiposCargo();
    	
    	if(tiposcargo != null) {
    		return Response.ok(tiposcargo).build();
    	}
    	return Response.status(NOT_FOUND).build();
    }
    
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") int id) {
		TipoCargo tipoCargo = tipoCargoBean.getTipoCargo(id);
		if (tipoCargo != null)
			return Response.ok(tipoCargo).build();
		return Response.status(NOT_FOUND).build();
	}
}
