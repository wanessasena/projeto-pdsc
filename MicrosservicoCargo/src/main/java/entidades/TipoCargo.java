/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author wanes
 */

@Entity
@Table (name="tipocargo", catalog = "musuario")
@Access(AccessType.FIELD)
public class TipoCargo implements Serializable{
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "nome", nullable = false, length = 200)
    private String nome;
    
    @Column(name = "horas_dia", nullable = false)
    private double horas_dia;
    
    @Column(name = "tempo_intervalo", nullable = false)
    private double tempo_intervalo;
    
    @Column(name = "ativo", nullable = true, columnDefinition = "boolean default true")
    private boolean ativo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getHoras_dia() {
        return horas_dia;
    }

    public void setHoras_dia(double horas_dia) {
        this.horas_dia = horas_dia;
    }

    public double getTempo_intervalo() {
        return tempo_intervalo;
    }

    public void setTempo_intervalo(double tempo_intervalo) {
        this.tempo_intervalo = tempo_intervalo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoCargo other = (TipoCargo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
}
