package api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import utils.JwTokenHelper;
import javax.ws.rs.client.Entity;
import javax.ws.rs.PathParam;

import entidades.Login;

@Path("/")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional
public class Gateway {

	//@JsonTokenNeeded
    @GET
    @Path("/all_users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allUsers() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoUsuario/api/users");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @GET
    @Path("/all_pontos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allTimeRegistrations() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoGestao/api/pontos");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @GET
    @Path("/all_pontos/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allTimeRegistrationsByUser(@PathParam ("token") String token) { 

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoGestao/api/pontos/token/" + token);
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @GET
    @Path("/all_departamentos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allDepartamentos() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoUsuario/api/departamentos");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @GET
    @Path("/all_tiposcargo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allTiposCargo() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoUsuario/api/tiposcargo");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @GET
    @Path("/all_binarios")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allBinarios() {

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoUsuario/api/binarios");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		return response;
    }
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(Login login) { 

    	String token = JwTokenHelper.getInstance().generateToken(login.getEmail(), login.getSenha());
    	login.setToken(token);
    	
    	System.out.println(token + login.getEmail() + login.getSenha());
    	
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/MicrosservicoUsuario/api/users/login");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON); 

		Response response = invocationBuilder.post(Entity.entity(login, MediaType.APPLICATION_JSON));
		
		String output = response.readEntity(String.class);
		
		return Response.ok(output).header(AUTHORIZATION, "Bearer " + token).build();
    }
    
}
