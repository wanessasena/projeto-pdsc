package usuario.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.transaction.Transactional;
import javax.ejb.EJB;
import usuario.ejb.BinarioBean;
import usuario.entidades.Binario;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;

@Path("/binarios")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional	
public class BinarioService {
	@EJB
    private BinarioBean binarioBean;

	@Context
	private HttpServletRequest httpRequest;
	
    @GET
    public Response allBinarios() {
    	List<Binario> binarios = binarioBean.getAllBinarios();
    	
    	if(binarios != null) {
    		return Response.ok(binarios).build();
    	}
    	return Response.status(NOT_FOUND).build();
    }
    
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") int id) {
		Binario binario = binarioBean.getBinario(id);
		if (binario != null)
			return Response.ok(binario).build();
		return Response.status(NOT_FOUND).build();
	}
}
