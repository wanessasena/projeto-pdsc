package usuario.api;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.transaction.Transactional;
import javax.ejb.EJB;
import usuario.ejb.UsuarioBean;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import usuario.entidades.EstruturaUsuario;
import usuario.entidades.Login;

import java.util.List;
import usuario.entidades.Usuario;

import javax.servlet.http.HttpServletRequest;

@Path("/users")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional	
public class UsuarioService {
	@EJB
    private UsuarioBean usuarioBean;

	@Context
	private HttpServletRequest httpRequest;
	
    @GET
    public Response allUsers() {
    	List<Usuario> users = usuarioBean.getAllUsers(); 
    	
    	if(users != null) {
    		return Response.ok(users).build();
    	}
    	return Response.status(NOT_FOUND).build();
    }
    
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") int id) {
		Usuario user = usuarioBean.getUser(id);
		if (user != null)
			return Response.ok(user).build();
		return Response.status(NOT_FOUND).build();
	}
	
	@GET
	@Path("/token/{token}")
	public Response findByToken(@PathParam("token") String token) {
		
		Usuario user = usuarioBean.getUserByToken(token);
		
		if (user!=null) {
			return Response.ok(user).build();
		}
		return Response.status(NOT_FOUND).build(); 
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(Login login) {
		
		Usuario user = usuarioBean.login(login.getEmail(), login.getSenha(), login.getToken());
		
		if (user!=null) {
			Login login_response = login;
			login_response.setUser_id(user.getId());
			
			return Response.ok(login_response).build();
		}
		return Response.status(NOT_FOUND).build(); 
	}
	
}
