package usuario.entidades;

public class Login {
	private long user_id;
	private String email;
	private String senha;
	private String token;
	private boolean isAdmin;
	
	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long id) {
		this.user_id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String login) {
		this.email = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public boolean getIsAdmin() {
		return isAdmin;
	}
	
	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}
