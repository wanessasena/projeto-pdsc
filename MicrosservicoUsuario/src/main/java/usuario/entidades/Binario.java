/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario.entidades;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author wanes
 */
@Entity
@Table (name="binario", catalog = "musuario")
@Access(AccessType.FIELD)
public class Binario implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
    private Usuario usuario_id;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "binario", nullable = false)
    private byte[] binario;
    
    @Column(name = "nome_foto", nullable = false, length = 200)
    private String nome_arquivo;
    
    @Column(name = "ativo", nullable = true, columnDefinition = "boolean default true")
    private boolean ativo;

}
