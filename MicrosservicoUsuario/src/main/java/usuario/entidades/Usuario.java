/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

/**
 *
 * @author wanes
 */

@Entity
@Table (name="usuario", catalog = "musuario")
@Access(AccessType.FIELD)
public class Usuario implements Serializable {
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "QRcode", nullable = false, length = 100, unique = true)
    private String qrcode;
    
    @Column(name = "token", nullable = false, length = 200, unique = true)
    private String token;
    
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;
        
    @Past
    @Temporal(TemporalType.DATE)	
    @Column(name="data_nascimento", nullable=true)
    private Date data_nascimento;
    
    @Column(name = "email", nullable = false, length = 45, unique = true)
    private String email;
    
	@JsonbTransient 
	@Column(name = "senha", nullable = false)
	private String senha;
	
    @Column(name = "tipo_cargo_id", nullable = false)
    private int tipo_cargo_id;
    
    @Column(name = "departamento_id", nullable = false)
    private int departamento_id;
    
    @Column(name = "administrador", nullable = true)
    private boolean administrador;
    
    @Column(name = "ativo", nullable = true, columnDefinition = "boolean default true")
    private boolean ativo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public int getTipo_cargo_id() {
        return tipo_cargo_id;
    }

    public void setTipo_cargo_id(int tipo_cargo_id) {
        this.tipo_cargo_id = tipo_cargo_id;
    }
    
    public int getDepartamento_id() {
        return tipo_cargo_id;
    }

    public void setDepartamento_id(int tipo_cargo_id) {
        this.tipo_cargo_id = tipo_cargo_id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 67 * hash + Objects.hashCode(this.token);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.token, other.token)) {
            return false;
        }
        return true;
    }
    
}
