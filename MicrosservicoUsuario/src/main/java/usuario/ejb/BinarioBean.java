package usuario.ejb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import usuario.entidades.Binario;

@Stateless
public class BinarioBean {
	@PersistenceContext(unitName = "pu")
	private EntityManager entityManager;
	
	@PostConstruct
    private void initializeBean(){
    }
	
	public List<Binario> getAllBinarios() {
		String jpql = ("select b from Binario b");
        Query query = entityManager.createQuery(jpql, Binario.class);
        List<Binario> binarios = query.getResultList();
        if (binarios !=null) {
        	return binarios;
        }
		return null;
	}
	
	public Binario getBinario(int id) {
		Binario binario = entityManager.find(Binario.class, id);
		if (binario != null)
            return binario;
		return null;
	} 
}
