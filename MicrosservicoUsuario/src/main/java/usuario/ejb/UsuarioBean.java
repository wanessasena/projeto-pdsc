package usuario.ejb;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import usuario.entidades.EstruturaUsuario;
import usuario.entidades.Usuario;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.ConstructorResult;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;

@Stateless	
public class UsuarioBean {
	@PersistenceContext(unitName = "pu")
	private EntityManager entityManager;

	@PostConstruct
    private void initializeBean(){
    }
	
	public List<Usuario> getAllUsers() {       
		Query query;
		query = entityManager.createNativeQuery(
				"SELECT * "
                + "FROM musuario.usuario u "
				+ "JOIN musuario.departamento d ON u.departamento_id = d.id "
                + "JOIN musuario.tipocargo t ON u.tipo_cargo_id = t.id", Usuario.class);
        
		List<Usuario> usuarios = query.getResultList();
        
        if (usuarios!=null) {
        	return usuarios;
        }
		return null;
	}
	
	public Usuario getUser(long id) {
		Usuario user = entityManager.find(Usuario.class, id);
		if (user != null)
            return user;
		return null;
	} 
	
	public Usuario login(String email, String senha, String token) {
        
		Query query;
		query = entityManager.createNativeQuery("SELECT * "
                + "FROM musuario.usuario u "
                + "WHERE u.email = '" + email
                + "' AND u.senha = '" + senha + "'", Usuario.class);
       
        Usuario usuario = (Usuario) query.getSingleResult();
        
        if(usuario != null) {
        	usuario.setToken(token);
        	updateUser(usuario);
        	usuario = getUser(usuario.getId());
        	return usuario;
        }

		return null;
	}
	
	public Usuario getUserByToken(String token) {
		Query query;
		query = entityManager.createNativeQuery("SELECT * "
                + "FROM musuario.usuario u "
                + "WHERE u.token = '" + token + "'", Usuario.class);
       
        Usuario usuario = (Usuario) query.getSingleResult();
		return usuario;
	}

	public void updateUser(Usuario user) {	
		entityManager.merge(user);
	}
}
