package gestao.api;

import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.transaction.Transactional;
import javax.ejb.EJB;
import gestao.ejb.TimeRegistrationBean;
import gestao.entidades.TimeRegistration;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import javax.servlet.http.HttpServletRequest;

@Path("/pontos")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional	
public class TimeRegistrationService {
	@EJB
    private TimeRegistrationBean timeRegistrationBean;

	@Context
	private HttpServletRequest httpRequest;
	
	@GET
	public Response findAllTimeRegistrations() {
		/*String authorizationHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader.substring("Bearer".length()).trim();
        System.out.println(token);*/

        List<TimeRegistration> time_reg_list = timeRegistrationBean.getAllTimeRegistrations();
		if (time_reg_list != null)
			return Response.ok(time_reg_list).build();
		return Response.status(NOT_FOUND).build();
	}
	
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") int id) {
		TimeRegistration time_reg = timeRegistrationBean.getTimeRegistration(id);
		if (time_reg != null)
			return Response.ok(time_reg).build();
		return Response.status(NOT_FOUND).build();
	}
	
	@GET
	@Path("/user/{userid}")
	public Response findByUserId(@PathParam("userid") int userId) {
		List<TimeRegistration> time_reg_list = timeRegistrationBean.getTimeRegistrationsByUserId(userId);
		if (time_reg_list != null)
			return Response.ok(time_reg_list).build();
		return Response.status(NOT_FOUND).build();
	}
	
	@GET
	@Path("/token/{token}")
	public Response findByUserToken(@PathParam("token") String token) {
		List<TimeRegistration> time_reg_list = timeRegistrationBean.getTimeRegistrationsByToken(token);
		if (time_reg_list != null)
			return Response.ok(time_reg_list).build();
		return Response.status(NOT_FOUND).build();
	}
	
}
