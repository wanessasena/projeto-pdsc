package gestao.ejb;

import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import gestao.entidades.TimeRegistration;

@Stateless	
public class TimeRegistrationBean {
	@PersistenceContext(unitName = "pu")
	private EntityManager entityManager;
	
	@PostConstruct
    private void initializeBean(){
    }
	
	public TimeRegistration getTimeRegistration(long id) {
		TimeRegistration time_reg = entityManager.find(TimeRegistration.class, id);
		if (time_reg != null)
            return time_reg;
		return null;
	} 
	
	public List<TimeRegistration> getAllTimeRegistrations() {
		Query query;
		query = entityManager.createNativeQuery(
				"SELECT * "
                + "FROM musuario.timeregistration t ", TimeRegistration.class);
        
		List<TimeRegistration> pontos = query.getResultList();
        
        if (pontos!=null) {
        	return pontos;
        }
		return null;
	}
	
	public List<TimeRegistration> getTimeRegistrationsByUserId(long userId){
		Query query;
		query = entityManager.createNativeQuery(
				"SELECT * "
                + "FROM musuario.timeregistration t "
                + "WHERE user_id = ?", TimeRegistration.class);
		
		query.setParameter(1, userId);
        
		List<TimeRegistration> pontos = query.getResultList();
        
        if (pontos!=null) {
        	return pontos;
        }
		return null;
	}
	
	public List<TimeRegistration> getTimeRegistrationsByToken(String token){
		Query query;
		query = entityManager.createNativeQuery(
				"SELECT * "
                + "FROM musuario.timeregistration t "
				+ "JOIN musuario.usuario u ON u.id = t.user_id "
                + "WHERE u.token = ?", TimeRegistration.class);
		
		query.setParameter(1, token);
        
		List<TimeRegistration> pontos = query.getResultList();
        
        if (pontos!=null) {
        	return pontos;
        }
		return null;
	}
	
	public TimeRegistration createTimeRegistration(long user_id, int order, Date created_on, double latitude, double longitude, String address) {
		TimeRegistration time_reg = new TimeRegistration();
		time_reg.setUser_id(user_id);
		time_reg.setOrder(order);
		time_reg.setCreated_on(created_on);
		time_reg.setLatitude(latitude);
		time_reg.setLongitude(longitude);
		time_reg.setAddress(address);
		entityManager.persist(time_reg);
		return time_reg;
	}
	
}
