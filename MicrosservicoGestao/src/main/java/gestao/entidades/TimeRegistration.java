/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ALUNO
 */

@Entity
@Table (name="timeregistration", catalog = "musuario")
@Access(AccessType.FIELD)
public class TimeRegistration implements Serializable{
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "user_id", nullable = false)
    private long user_id;
    
    @Column(name = "order", nullable = false)
    private int order;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date created_on;
    
    @Column(name = "latitude", nullable = false)
    private double latitude;
    
    @Column(name = "longitude", nullable = false)
    private double longitude;
    
    @Column(name = "address", nullable = false, length = 500)
    private String address;

    @Column(name = "updated_by", nullable = false)
    private long updated_by;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updated_on;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Date getCreated_on() {
        return created_on;
    }
    
    public void setUpdated_by(int updated_by) {
        this.updated_by = updated_by;
    }

    public long getUpdated_by() {
        return updated_by;
    } 

    public void setUpdated_on(Date updated_on) {
        this.updated_on = updated_on;
    }

    public Date getUpdated_on() {
        return updated_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimeRegistration other = (TimeRegistration) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
}
