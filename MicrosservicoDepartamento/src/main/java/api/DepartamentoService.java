package api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.transaction.Transactional;
import javax.ejb.EJB;
import ejb.DepartamentoBean;
import entidades.Departamento;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;

@Path("/departamentos")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Transactional	
public class DepartamentoService {
	@EJB
    private DepartamentoBean departamentoBean;

	@Context
	private HttpServletRequest httpRequest;
	
    @GET
    public Response allDepartamentos() {
    	List<Departamento> departamento = departamentoBean.getAllDepartamentos();
    	
    	if(departamento != null) {
    		return Response.ok(departamento).build();
    	}
    	return Response.status(NOT_FOUND).build();
    }
    
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") int id) {
		Departamento departamento = departamentoBean.getDepartamento(id);
		if (departamento != null)
			return Response.ok(departamento).build();
		return Response.status(NOT_FOUND).build();
	}
}
