package ejb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entidades.Departamento;

@Stateless
public class DepartamentoBean {
	@PersistenceContext(unitName = "pu")
	private EntityManager entityManager;
	
	@PostConstruct
    private void initializeBean(){
    }
	
	public List<Departamento> getAllDepartamentos() {
		String jpql = ("select d from Departamento d");
        Query query = entityManager.createQuery(jpql, Departamento.class);
        List<Departamento> tiposcargo = query.getResultList();
        if (tiposcargo !=null) {
        	return tiposcargo;
        }
		return null;
	}
	
	public Departamento getDepartamento(int id) {
		Departamento departamento = entityManager.find(Departamento.class, id);
		if (departamento != null)
            return departamento;
		return null;
	} 
}
